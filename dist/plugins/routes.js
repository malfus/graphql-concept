'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
function register(server, options, next) {
    server.route({
        method: 'GET',
        path: '/suck-town',
        config: {
            handler: function handler(request, reply) {
                reply({
                    x: 42
                });
            }
        }
    });
    next();
};
register.attributes = {
    name: 'gql-routes',
    version: '1.0.0'
};
exports.default = register;