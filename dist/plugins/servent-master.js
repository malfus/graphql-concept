'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _hoek = require('hoek');

var _hoek2 = _interopRequireDefault(_hoek);

var _events = require('events');

var _events2 = _interopRequireDefault(_events);

function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : { default: obj };
}

/**
 * Created by jhenley on 2/14/2017.
 */
function register(server, options, next) {
    var emitter = new _events2.default();
    server.on('tail', function (req) {
        var servent = {
            verb: _hoek2.default.reach(req, 'method'),
            status: _hoek2.default.reach(req, 'response.statusCode'),
            host: _hoek2.default.reach(req, 'info.host'),
            path: req.path,
            hapi_id: req.id,
            req_headers: _hoek2.default.clone(_hoek2.default.reach(req, 'headers', { default: {} })),
            res_headers: _hoek2.default.clone(_hoek2.default.reach(req, 'response.headers', { default: {} })),

            status_class: Math.trunc(_hoek2.default.reach(req, 'response.statusCode', { default: 0 }) / 100) + '00s',
            timestamp: new _hoek2.default.Timer().ts / 1000,
            datestamp: new Date(Date.now()).toISOString()
        };

        emitter.emit('servent', servent);
    });
    // server.expose('serventEmitter', () => emitter);
    server.decorate('server', 'serventEmitter', emitter);
    next();
};
register.attributes = {
    name: 'servent-master',
    version: '1.0.0'
};
exports.default = register;