"use strict";

var apiServiceGenerator_1 = require("./generator/apiServiceGenerator");
/**
 * Created by jhenley on 3/16/2017.
 */
var services = apiServiceGenerator_1.generateService('ADDRESS_URI', 'https://dev.servsmartapi.com/V4/Locations/Addresses');
exports.addressService = services;
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = exports.addressService;