"use strict";
/**
 * Created by jhenley on 3/16/2017.
 */

var apiServiceGenerator_1 = require("./generator/apiServiceGenerator");
var services = apiServiceGenerator_1.generateService('LOCATION_URI', 'https://dev.servsmartapi.com/V4/Locations/Locations');
var organization = services;
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = organization;