"use strict";
/**
 * Created by jhenley on 3/15/2017.
 */

var Request = require("request");
function doAzureSearch(brand, zipCode, callback) {
    var url = "https://sm-fsg-organizations-lab.search.windows.net/indexes/" + brand + "-coverage/docs/search?api-version=2016-09-01";
    return new Promise(function (resolve, reject) {
        Request.post({
            uri: url,
            body: {
                search: zipCode,
                count: true,
                searchMode: 'all'
            },
            json: true,
            headers: {
                'api-key': '638AD736CAD50CAB1D65E987B5BA0300'
            }
        }, function (err, httpResponse, body) {
            if (err) {
                reject(err);
                return;
            }
            var val = body.value;
            if (typeof callback === 'function') {
                callback(val);
            }
            resolve(val);
        });
    });
}
exports.service = {
    doAzureSearch: doAzureSearch
};