"use strict";
/**
 * Created by jhenley on 2/24/2017.
 */
/**
 * Created by jhenley on 2/15/2017.
 */

var Request = require("request");
var BASE_URI = process.env.PROGRAM_URI || 'https://dev.servsmartapi.com/V3/Programs/ProgramDefinitions';
var getById = function getById(id, callback) {
    var idPath = "/" + id;
    return new Promise(function (resolve, reject) {
        Request(BASE_URI + idPath, function (err, res, body) {
            if (err) {
                reject(err);
                return;
            }
            var val = JSON.parse(body).Data;
            if (typeof callback === 'function') {
                callback(val);
            }
            resolve(val);
        });
    });
};
var getAll = function getAll(callback) {
    return new Promise(function (resolve, reject) {
        Request(BASE_URI, function (err, res, body) {
            if (err) {
                reject(err);
                return;
            }
            var val = JSON.parse(body).Data;
            if (typeof callback === 'function') {
                callback(val);
            }
            resolve(val);
        });
    });
};
var getByFilter = function getByFilter() {
    var args = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
    var callback = arguments[1];

    var uri = BASE_URI;
    return new Promise(function (resolve, reject) {
        Request(uri, { qs: args }, function (err, res, body) {
            if (err) {
                reject(err);
                return;
            }
            var val = JSON.parse(body).Data;
            if (typeof callback === 'function') {
                callback(val);
            }
            resolve(val);
        });
    });
};
var program = {
    getById: getById,
    getAll: getAll,
    getByFilter: getByFilter
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = program;