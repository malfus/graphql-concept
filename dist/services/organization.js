"use strict";

var apiServiceGenerator_1 = require("./generator/apiServiceGenerator");
var services = apiServiceGenerator_1.generateService('ORGANIZATION_URI', 'https://dev.servsmartapi.com/V4/Organizations');
var organization = services;
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = organization;