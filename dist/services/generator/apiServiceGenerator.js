"use strict";
/**
 * Created by jhenley on 3/7/2017.
 */

var Request = require("request");
function generateService(envURI, defaultURI) {
    var BASE_URI = process.env[envURI] || defaultURI;
    var getById = function getById(id, callback) {
        if (!id) {
            return null;
        }
        var idPath = "/" + id;
        return new Promise(function (resolve, reject) {
            Request(BASE_URI + idPath, function (err, res, body) {
                if (err) {
                    reject(err);
                    return;
                }
                var val = JSON.parse(body).Data;
                if (typeof callback === 'function') {
                    callback(val);
                }
                resolve(val);
            });
        });
    };
    var getAll = function getAll(callback) {
        return new Promise(function (resolve, reject) {
            Request(BASE_URI, function (err, res, body) {
                if (err) {
                    reject(err);
                    return;
                }
                var val = JSON.parse(body).Data;
                if (typeof callback === 'function') {
                    callback(val);
                }
                resolve(val);
            });
        });
    };
    var getByFilter = function getByFilter() {
        var args = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
        var callback = arguments[1];

        var uri = BASE_URI;
        return new Promise(function (resolve, reject) {
            Request(uri, { qs: args }, function (err, res, body) {
                if (err) {
                    reject(err);
                    return;
                }
                var val = JSON.parse(body).Data;
                if (typeof callback === 'function') {
                    callback(val);
                }
                resolve(val);
            });
        });
    };
    return {
        getById: getById,
        getAll: getAll,
        getByFilter: getByFilter
    };
}
exports.generateService = generateService;