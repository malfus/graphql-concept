"use strict";
/**
 * Created by jhenley on 2/23/2017.
 */

var resolverMap = {
    ProgramDefinition: {
        name: function name() {
            return 'PLACEHOLDER';
        }
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = resolverMap;