"use strict";
/**
 * Created by jhenley on 2/22/2017.
 */

exports.resolverMap = {
    Attribute: {
        name: function name(obj) {
            return obj.Name;
        },
        value: function value(obj) {
            return obj.Value;
        }
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = exports.resolverMap;