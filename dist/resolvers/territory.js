"use strict";

var Hoek = require("hoek");
var relatedEntities_1 = require("./relatedEntities");
/**
 * Created by jhenley on 2/22/2017.
 */
var resolveAssignedEntities = function resolveAssignedEntities(obj, args, context) {
    return obj.AssignedEntities.map(function (assignedEntity) {
        return relatedEntities_1.resolveEntity(assignedEntity, args, context);
    });
};
var resolverMap = {
    Territory: {
        id: function id(obj) {
            return obj.TerritoryID;
        },
        assignedEntities: resolveAssignedEntities
    },
    Boundary: {},
    Query: {
        territoryByID: function territoryByID(obj, _ref, context) {
            var id = _ref.id;
            return context.Territory.getById(id);
        },
        territories: function territories(obj, args, context) {
            return context.Territory.getByFilter(args);
        }
    }
};
Hoek.merge(resolverMap, relatedEntities_1.resolverMap);
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = resolverMap;