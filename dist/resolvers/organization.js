"use strict";

var atttribute_1 = require("./atttribute");
var legacyInformation_1 = require("./legacyInformation");
var Hoek = require("hoek");
/**
 * Created by jhenley on 2/15/2017.
 */
var getOrganizationById = function getOrganizationById(id, context) {
    return context.Organization.getById(id);
};
var getOrganizations = function getOrganizations(obj, args, context) {
    return context.Organization.getByFilter(args);
};
var getAttributeValueForOrganization = function getAttributeValueForOrganization(attributeName) {
    return function (obj) {
        var attribute = obj.Attributes.filter(function (attribute) {
            return attribute.Name === attributeName;
        })[0];
        return attribute ? attribute.Value : null;
    };
};
var resolverMap = {
    Organization: {
        id: function id(_ref) {
            var OrganizationID = _ref.OrganizationID;
            return OrganizationID;
        },
        name: function name(_ref2) {
            var LegalName = _ref2.LegalName;
            return LegalName;
        },
        parentOrganization: function parentOrganization(_ref3, args, context) {
            var ParentOrganizationID = _ref3.ParentOrganizationID;
            return getOrganizationById(ParentOrganizationID, context);
        },
        legacyName: function legacyName(obj) {
            return obj.LegacyInformation ? obj.LegacyInformation.LegacySystemName : null;
        },
        legacyID: function legacyID(obj) {
            return obj.LegacyInformation ? obj.LegacyInformation.LegacySystemIdentifier : null;
        },
        trades: getAttributeValueForOrganization('trades'),
        locations: function locations(obj, args, context) {
            return context.Location.getByFilter({ fRelatedOrganizationID: obj.OrganizationID });
        },
        addresses: function addresses(obj, args, context) {
            return new Promise(function (resolve1, reject1) {
                context.Location.getByFilter({ fRelatedOrganizationID: obj.OrganizationID }).then(function (data1) {
                    return resolve1(Promise.all(data1.map(function (loc) {
                        return new Promise(function (resolve, reject) {
                            var addPromise = context.Address.getById(loc.AddressID);
                            addPromise.then(function (data) {
                                return resolve(data[0]);
                            });
                            addPromise.catch(function (err) {
                                return reject(err);
                            });
                        });
                    })));
                });
            });
        }
    }, Query: {
        organizationByID: function organizationByID(obj, args, context) {
            return getOrganizationById(args.id, context);
        },
        organizations: getOrganizations
    }
};
Hoek.merge(resolverMap, atttribute_1.default);
Hoek.merge(resolverMap, legacyInformation_1.default);
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = resolverMap;