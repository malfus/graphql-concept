"use strict";
/**
 * Created by jhenley on 3/15/2017.
 */

exports.resolverMap = {
    AzureOrganization: {
        score: function score(obj) {
            return obj['@search.score'];
        }
    },
    Query: {
        azureSearch: function azureSearch(obj, args, context) {
            return context.AzureSearch.doAzureSearch(args.brand, args.zipCode);
        }
    }
};