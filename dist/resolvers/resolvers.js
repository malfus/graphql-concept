"use strict";
/**
 * Created by jhenley on 2/23/2017.
 */

var Hoek = require("hoek");
var territory_1 = require("./territory");
var organization_1 = require("./organization");
var program_1 = require("./program");
var azureSearch_1 = require("./azureSearch");
var location_1 = require("./location");
var address_1 = require("./address");
exports.resolverMap = {};
Hoek.merge(exports.resolverMap, organization_1.default);
Hoek.merge(exports.resolverMap, territory_1.default);
Hoek.merge(exports.resolverMap, program_1.default);
Hoek.merge(exports.resolverMap, azureSearch_1.resolverMap);
Hoek.merge(exports.resolverMap, location_1.resolverMap);
Hoek.merge(exports.resolverMap, address_1.addressResolvers);
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = exports.resolverMap;