"use strict";
/**
 * Created by jhenley on 2/22/2017.
 */

function resolveEntity(obj, args, context) {
    if (obj.EntityType === 'License') {
        return context.Organization.getById(obj.EntityID);
    } else if (obj.EntityType === 'ProgramDefinition') {
        return context.Program.getById(obj.EntityID);
    } else {
        return {};
    }
}
exports.resolveEntity = resolveEntity;
;
var resolveType = function resolveType(obj) {
    if (obj.DoingBusinessAsName) return 'Organization';else if (obj.ProgramDefinitionID) return 'ProgramDefinition';else return 'Unknown';
};
exports.resolverMap = {
    RelatedOrganization: {
        id: function id(obj) {
            return obj.OrganizationID;
        },
        type: function type(obj) {
            return obj.RelationshipType;
        },
        organization: function organization(obj, args, context) {
            return context.Organization.getById(obj.OrganizationID);
        }
    },
    AssignedEntity: {
        entity: resolveEntity
    },
    Entity: {
        __resolveType: resolveType
    },
    Unknown: {
        message: function message() {
            return 'Unknown entity';
        }
    }
};