"use strict";
/**
 * Created by jhenley on 2/23/2017.
 */

var resolverMap = {
    ProgramDefinition: {
        id: function id(obj) {
            return obj.ProgramDefinitionID;
        },
        name: function name(obj) {
            return obj.Name;
        },
        description: function description(obj) {
            return obj.Description;
        },
        administeringOrganization: function administeringOrganization(obj, args, context) {
            return context.Organization.getById(obj.AdministeringOrganizationID);
        },
        sponsorOrganization: function sponsorOrganization(obj, args, context) {
            return context.Organization.getById(obj.SponsorOrganizationID);
        }
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = resolverMap;