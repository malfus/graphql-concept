"use strict";
/**
 * Created by jhenley on 2/22/2017.
 */

var resolverMap = {
    LegacyInformation: {
        name: function name(obj) {
            return obj.LegacySystemName;
        },
        id: function id(obj) {
            return obj.LegacySystemIdentifier;
        }
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = resolverMap;