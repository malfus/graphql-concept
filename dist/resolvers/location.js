"use strict";

var Hoek = require("hoek");
var relatedEntities_1 = require("./relatedEntities");
var atttribute_1 = require("./atttribute");
/**
 * Created by jhenley on 3/16/2017.
 */
exports.resolverMap = {
    Location: {
        id: function id(obj) {
            return obj.LocationID;
        },
        relatedOrganizations: function relatedOrganizations(obj, args, context) {
            return obj.RelatedOrganizations.map(function (relatedOrg) {
                return context.Organization.getById(relatedOrg.OrganizationID);
            });
        },
        address: function address(obj, args, context) {
            return new Promise(function (resolve, reject) {
                var addPromise = context.Address.getById(obj.AddressID);
                addPromise.then(function (data) {
                    return resolve(data[0]);
                });
                addPromise.catch(function (err) {
                    return reject(err);
                });
            });
        }
    }
};
Hoek.merge(exports.resolverMap, relatedEntities_1.resolverMap);
Hoek.merge(exports.resolverMap, atttribute_1.resolverMap);