"use strict";

var _hapi = require("hapi");

var _hapi2 = _interopRequireDefault(_hapi);

var _good = require("good");

var _good2 = _interopRequireDefault(_good);

var _resolvers = require("./resolvers/resolvers");

var _resolvers2 = _interopRequireDefault(_resolvers);

var _organization = require("./services/organization");

var _organization2 = _interopRequireDefault(_organization);

var _territory = require("./services/territory");

var _territory2 = _interopRequireDefault(_territory);

var _program = require("./services/program");

var _program2 = _interopRequireDefault(_program);

var _location = require("./services/location");

var _location2 = _interopRequireDefault(_location);

var _good3 = require("./config/good");

var _good4 = _interopRequireDefault(_good3);

var _azureSearch = require("./services/azureSearch");

var _graphqlTools = require("graphql-tools");

var _apolloServer = require("apollo-server");

var _blipp = require("blipp");

var _blipp2 = _interopRequireDefault(_blipp);

var _address = require("./services/address");

function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : { default: obj };
}

/**
 * Created by jhenley on 2/14/2017.
 */
var schema = "type Address {\n  id: ID!\n  AddressType: String\n  ID: ID!\n  Address1: String\n  Address2: String\n  City: String\n  State: String\n  PostalCode: String\n  Country: String\n  FullAddress: String\n  ValidationError: String\n  VerifyLevel: String\n  Prompt: String\n  Metadata: AddressMetaData\n}\n\ntype AddressMetaData {\n  Precision: String\n  Location: LatLong\n}\n\ntype LatLong {\n  Latitude: String\n  Longitude: String\n}\n\ntype Attribute {\n  name: String!\n  Name: String!\n  value: String\n  Value: String\n}\n\ntype AzureOrganization {\n  score: Float\n  id: ID\n  enterpriseNumber: String\n  licenseNumber: String\n  enterpriseDBA: String\n  licenseDBA: String\n  legalName: String\n  address: String\n  city: String\n  state: String\n  zip: String\n  phone: String\n  extension: String\n  url: String\n}\n\ntype Query {\n  azureSearch(brand: String, zipCode: String): [AzureOrganization]\n  organizationByID(id: ID): Organization\n  organizationByID(id: ID): Territory\n  organizations(fLegalName: String, fLegacyInformation_LegacySystemName: String, fLegacyInformation_LegacySystemIdentifier: String, fAttributeName: String, fAttributeValue: String, sLegalName: String, sLegacyInformation_LegacySystemName: String, sLegacyInformation_LegacySystemIdentifier: String, pStart: String, pEnd: String): [Organization]\n  territoryByID(id: ID): Territory\n  territories(fTerritoryPostalCode: String, fAssignedEntities_EntityID: String, fRelatedOrganizations_OrganizationID: String, pStart: String, pEnd: String): [Territory]\n}\n\ntype Boundary {\n  BoundaryDefData: [String]\n  BoundaryDefType: String\n  BoundaryDefType: String\n  BoundaryDefData: [String]!\n}\n\ntype LegacyInformation {\n  name: String\n  id: ID\n  LegacySystemName: String\n  LegacySystemIdentifier: ID\n}\n\ntype Location {\n  id: ID!\n  LocationID: ID!\n  Name: String\n  Type: String\n  AddressID: ID\n  address: Address\n  LastModifiedDate: String\n  RelatedOrganizations: [RelatedOrganization]\n  relatedOrganizations: [Organization]\n  Attributes: [Attribute]\n}\n\ntype Organization {\n  id: ID!\n  name: String!\n  parentOrganization: Organization\n  OrganizationID: ID!\n  LegalName: String!\n  DoingBusinessAsName: String\n  IsExternal: Boolean\n  MainType: String\n  SubType: String\n  InternalAliasName: String\n  Status: String\n  LastModifiedDate: String\n  ParentOrganizationID: ID\n  LegacyInformation: LegacyInformation\n  legacyName: String\n  legacyID: ID\n  trades: String\n  Attributes: [Attribute]!\n  locations: [Location]!\n  addresses: [Address]!\n}\n\ntype ProgramDefinition {\n  id: ID!\n  name: String\n  description: String\n  ProgramDefinitionID: ID\n  Name: String\n  Description: String\n  AdministeringOrganizationID: ID\n  administeringOrganization: Organization\n  SponsorOrganizationID: ID\n  sponsorOrganization: Organization\n  RequiredForProductIDs: [ID]\n  QualificationNames: [String]\n  LastModifiedDate: String\n}\n\ntype RelatedOrganization {\n  id: ID!\n  type: String!\n  OrganizationID: ID!\n  RelationshipType: String!\n  organization: Organization\n}\n\ntype AssignedEntity {\n  EntityType: String\n  EntityID: ID\n  TerritoryType: String\n  entity: Entity\n}\n\ntype Unknown {\n  message: String\n}\n\nunion Entity = Organization | ProgramDefinition | Unknown\n\ntype Territory {\n  id: ID!\n  TerritoryID: ID!\n  Description: String\n  LastModifiedDate: String\n  LegacyInformation: LegacyInformation\n  RelatedOrganizations: [RelatedOrganization]\n  AssignedEntities: [AssignedEntity]\n  assignedEntities: [Entity]\n  Boundaries: [Boundary]\n}\n\n";

var port = process.env.PORT || 4242;
var host = process.env.HOST || 'localhost';
var server = new _hapi2.default.Server({});
server.connection({
    port: port,
    host: host
});

var context = {
    Organization: _organization2.default,
    Territory: _territory2.default,
    Program: _program2.default,
    AzureSearch: _azureSearch.service,
    Location: _location2.default,
    Address: _address.addressService
};
// var graphQLSchema = buildSchema(schema);
//
// addResolveFunctionsToSchema(graphQLSchema, resolvers);

var executableSchema = (0, _graphqlTools.makeExecutableSchema)({
    typeDefs: [schema],
    resolvers: _resolvers2.default
});
server.register({
    register: _apolloServer.apolloHapi,
    options: {
        path: '/graphql',
        apolloOptions: function apolloOptions() {
            return {
                pretty: true,
                schema: executableSchema,
                context: context
            };
        }
    }
});

server.register({
    register: _apolloServer.graphiqlHapi,
    options: {
        path: '/graphiql',
        graphiqlOptions: {
            endpointURL: '/graphql'
        }
    }
});
server.register([{
    register: _good2.default,
    options: _good4.default
},
// {
//     register: GraphQL,
//     options: {
//         route: {
//             path: '/graphql',
//             config: {}
//         },
//         query: {
//             schema: graphqlSchema,
//             context,
//             graphiql: true
//         }
//     }
// }
{
    register: _blipp2.default,
    options: {}
}], function (err) {
    return server.start(function (err) {
        console.log("server started at " + server.info.uri);
        // server.serventEmitter.on('servent', servent => {
        //     console.dir(servent);
        // })
    });
});