'use strict';

var _request = require('request');

var _request2 = _interopRequireDefault(_request);

function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : { default: obj };
}

var count = 5;
var buffer = [];
while (count-- > 0) {
    doWork(count);
}
function doWork(count) {
    var item = buffer.pop();
    if (item) {
        (0, _request2.default)(uri, function (err, res, body) {
            doStuff(body);
            doWork(count);
        });
    } else {
        console.log('Thread ' + count + ' done');
    }
}