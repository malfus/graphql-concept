'use strict';

var _request = require('request');

var _request2 = _interopRequireDefault(_request);

function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : { default: obj };
}

var BASE_URI = process.env.ORGANIZATION_URI || 'https://dev.servsmartapi.com/V4/Organizations'; /**
                                                                                                 * Created by jhenley on 2/14/2017.
                                                                                                 */

var getById = function getById(id, callback) {
    var idPath = '/' + id;
    Wreck.get(BASE_URI + idPath, function (err, res, payload) {
        return console.dir(payload);
    });
};
var getAll = function getAll(callback) {
    (0, _request2.default)(BASE_URI, function (err, res, body) {
        console.dir(JSON.parse(body).Data);
    });
};
getAll();