/**
 * Created by jhenley on 3/15/2017.
 */
import * as Request from 'request'
import {AzureOrganization} from "../types/AzureOrganization";
function doAzureSearch(brand: string, zipCode: string, callback: Function) {
    const url = `https://sm-fsg-organizations-lab.search.windows.net/indexes/${brand}-coverage/docs/search?api-version=2016-09-01`;
    return new Promise((resolve, reject) => {
        Request.post({
                uri: url,
                body: {
                    search: zipCode,
                    count: true,
                    searchMode: 'all'
                },
                json: true,
                headers: {
                    'api-key': '-----------------'
                }
            }, (err, httpResponse, body) => {
                if (err) {
                    reject(err);
                    return;
                }
                let val = body.value;
                if (typeof callback === 'function') {
                    callback(val);
                }
                resolve(val);
            }
        )
    });
}
export var service: AzureSearchService = {
    doAzureSearch
};
export interface AzureSearchService {
    doAzureSearch: (brand: string, zipCode: string, callback?: Function) => Promise<AzureOrganization>
}