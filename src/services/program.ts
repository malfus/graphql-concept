/**
 * Created by jhenley on 2/24/2017.
 */
/**
 * Created by jhenley on 2/15/2017.
 */
import * as Request from 'request'
import {ProgramDefinition, ProgramDefinitionFilter} from "../types/Program";
const BASE_URI = process.env.PROGRAM_URI || 'https://dev.servsmartapi.com/V3/Programs/ProgramDefinitions';
const getById = (id: string, callback?: Function): Promise<ProgramDefinition> => {
    let idPath = `/${id}`;
    return new Promise((resolve, reject) => {
        Request(BASE_URI + idPath, (err, res, body) => {
            if (err) {
                reject(err);
                return;
            }
            let val = JSON.parse(body).Data;
            if (typeof callback === 'function') {
                callback(val);
            }
            resolve(val);
        })
    });
};
const getAll = (callback?: Function): Promise<ProgramDefinition> => {
    return new Promise((resolve, reject) => {
        Request(BASE_URI, (err, res, body) => {
            if (err) {
                reject(err);
                return;
            }
            let val = JSON.parse(body).Data;
            if (typeof callback === 'function') {
                callback(val);
            }
            resolve(val);
        })
    });
};
const getByFilter = (args: ProgramDefinitionFilter={ }, callback?: Function): Promise<ProgramDefinition> => {
    let uri = BASE_URI;
    return new Promise((resolve, reject) => {
        Request(uri, { qs: args }, (err, res, body) => {
            if (err) {
                reject(err);
                return;
            }
            let val = JSON.parse(body).Data;
            if (typeof callback === 'function') {
                callback(val);
            }
            resolve(val);
        })
    });
};

const program: ProgramService = {
    getById,
    getAll,
    getByFilter
};
export default program;
export interface ProgramService {
    getById: (id: string, callback?: Function) => Promise<ProgramDefinition>,
    getAll: (callback?: Function) => Promise<ProgramDefinition>,
    getByFilter: (args: ProgramDefinitionFilter, callback?: Function) => Promise<ProgramDefinition>
}