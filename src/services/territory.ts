import {Territory, TerritoryFilter} from "../types/Territory";
/**
 * Created by jhenley on 2/22/2017.
 */
var Request = require('request');
const BASE_URI = process.env.TERRITORY_URI || 'https://dev.servsmartapi.com/V4/Territories';
const getById = (id: string, callback: Function): Promise<Territory> => {
    let idPath = `/${id}`;
    return new Promise((resolve, reject) => {
        Request(BASE_URI + idPath, (err, res, body) => {
            if (err) {
                reject(err);
                return;
            }
            let val = JSON.parse(body).Data;
            if (typeof callback === 'function') {
                callback(val);
            }
            resolve(val);
        })
    });
};
const getAll = (callback: Function): Promise<Territory> => {
    return new Promise((resolve, reject) => {
        Request(BASE_URI, (err, res, body) => {
            if (err) {
                reject(err);
                return;
            }
            let val = JSON.parse(body).Data;
            if (typeof callback === 'function') {
                callback(val);
            }
            resolve(val);
        })
    });
};
const getByFilter = (args: TerritoryFilter={ }, callback: Function): Promise<Territory> => {
    let uri = BASE_URI;
    return new Promise((resolve, reject) => {
        Request(uri, { qs: args }, (err, res, body) => {
            if (err) {
                reject(err);
                return;
            }
            let val = JSON.parse(body).Data;
            if (typeof callback === 'function') {
                callback(val);
            }
            resolve(val);
        })
    });
};
var territory = {
    getById,
    getAll,
    getByFilter
};
module.exports = territory;
export interface TerritoryService {
    getById: (string, Function?) => Promise<Territory>,
    getAll: (Function?) => Promise<Territory>,
    getByFilter: (TerritoryFilter, Function?) => Promise<Territory>
}