/**
 * Created by jhenley on 3/7/2017.
 */
import * as Request from 'request'
export function generateService(envURI: string, defaultURI: string) {
    const BASE_URI = process.env[envURI] || defaultURI;
    const getById = (id: string, callback: Function): Promise<any> => {
        if (!id) {
            return null;
        }
        let idPath = `/${id}`;
        return new Promise((resolve, reject) => {
            Request(BASE_URI + idPath, (err, res, body) => {
                if (err) {
                    reject(err);
                    return;
                }
                let val = JSON.parse(body).Data;
                if (typeof callback === 'function') {
                    callback(val);
                }
                resolve(val);
            })
        });
    };
    const getAll = (callback: Function): Promise<any> => {
        return new Promise((resolve, reject) => {
            Request(BASE_URI, (err, res, body) => {
                if (err) {
                    reject(err);
                    return;
                }
                let val = JSON.parse(body).Data;
                if (typeof callback === 'function') {
                    callback(val);
                }
                resolve(val);
            })
        });
    };
    const getByFilter = (args = {}, callback: Function): Promise<any> => {
        let uri = BASE_URI;
        return new Promise((resolve, reject) => {
            Request(uri, {qs: args}, (err, res, body) => {
                if (err) {
                    reject(err);
                    return;
                }
                let val = JSON.parse(body).Data;
                if (typeof callback === 'function') {
                    callback(val);
                }
                resolve(val);
            })
        });
    };
    return {
        getById,
        getAll,
        getByFilter
    }
}