import {AddressFilter, Address} from "../types/Address";
import {generateService} from "./generator/apiServiceGenerator";
/**
 * Created by jhenley on 3/16/2017.
 */
const services = generateService('ADDRESS_URI', 'https://dev.servsmartapi.com/V4/Locations/Addresses');

export const addressService: AddressService = services;
export default addressService;
export interface AddressService {
    getById: (id: string, callback?: Function) => Promise<Address[]>,
    getAll: (callback?: Function) => Promise<Address[]>,
    getByFilter: (args: AddressFilter, callback?: Function) => Promise<Address[]>
}