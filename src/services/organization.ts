/**
 * Created by jhenley on 2/15/2017.
 */
import {Organization, OrgFilter} from "../types/Organization";
import {generateService} from "./generator/apiServiceGenerator";
const services = generateService('ORGANIZATION_URI', 'https://dev.servsmartapi.com/V4/Organizations');

const organization: OrganizationService = services;
export default organization;
export interface OrganizationService {
    getById: (id: string, callback?: Function) => Promise<Organization>,
    getAll: (callback?: Function) => Promise<Organization>,
    getByFilter: (agrs: OrgFilter, callback?: Function) => Promise<Organization>
}