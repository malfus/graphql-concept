/**
 * Created by jhenley on 3/16/2017.
 */
import {generateService} from "./generator/apiServiceGenerator";
import {LocationFilter, Location} from "../types/Location";
const services = generateService('LOCATION_URI', 'https://dev.servsmartapi.com/V4/Locations/Locations');

const organization: LocationService = services;
export default organization;
export interface LocationService {
    getById: (id: string, callback?: Function) => Promise<Location>,
    getAll: (callback?: Function) => Promise<Location[]>,
    getByFilter: (args: LocationFilter, callback?: Function) => Promise<Location[]>
}