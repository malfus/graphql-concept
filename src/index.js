/**
 * Created by jhenley on 2/14/2017.
 */
import Hapi from "hapi";
import Good from "good";
import schema from "./lib/schema.graphql";
import resolvers from "./resolvers/resolvers";
import Organization from "./services/organization";
import Territory from "./services/territory";
import Program from "./services/program";
import Location from "./services/location";
import good_options from "./config/good";
import {service as AzureSearch} from "./services/azureSearch";
import {addResolveFunctionsToSchema, makeExecutableSchema} from "graphql-tools";
import {apolloHapi, graphiqlHapi} from "apollo-server";
import Blipp from "blipp";
import {addressService as Address} from "./services/address";

const port = process.env.PORT || 4242;
const host = process.env.HOST || 'localhost';
const server = new Hapi.Server({});
server.connection({
    port,
    host
});

const context = {
    Organization,
    Territory,
    Program,
    AzureSearch,
    Location,
    Address
};
// var graphQLSchema = buildSchema(schema);
//
// addResolveFunctionsToSchema(graphQLSchema, resolvers);

let executableSchema = makeExecutableSchema({
    typeDefs: [schema],
    resolvers: resolvers
});
server.register({
    register: apolloHapi,
    options: {
        path: '/graphql',
        apolloOptions: () => ({
            pretty: true,
            schema: executableSchema,
            context
        }),
    },
});

server.register({
    register: graphiqlHapi,
    options: {
        path: '/graphiql',
        graphiqlOptions: {
            endpointURL: '/graphql',
        },
    },
});
server.register([
        {
            register: Good,
            options: good_options
        },
        // {
        //     register: GraphQL,
        //     options: {
        //         route: {
        //             path: '/graphql',
        //             config: {}
        //         },
        //         query: {
        //             schema: graphqlSchema,
        //             context,
        //             graphiql: true
        //         }
        //     }
        // }
        {
            register: Blipp,
            options: {}
        }

    ], err =>
        server.start(err => {
            console.log(`server started at ${server.info.uri}`)
            // server.serventEmitter.on('servent', servent => {
            //     console.dir(servent);
            // })
        })
);