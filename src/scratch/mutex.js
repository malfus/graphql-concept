import Request from 'request'
let count = 5;
var buffer = [];
while (count-- > 0) {
    doWork(count);
}
function doWork(count) {
    var item = buffer.pop();
    if (item) {
        Request(uri, (err, res, body) => {
            doStuff(body);
            doWork(count);
        });
    } else {
        console.log(`Thread ${count} done`);
    }
}