/**
 * Created by jhenley on 2/14/2017.
 */
import Request from 'request'
const BASE_URI = process.env.ORGANIZATION_URI || 'https://dev.servsmartapi.com/V4/Organizations';
const getById = (id, callback) => {
    let idPath = `/${id}`;
    Wreck.get(BASE_URI + idPath, (err, res, payload) => console.dir(payload));
};
const getAll = callback => {
    Request(BASE_URI, (err, res, body) => {
        console.dir(JSON.parse(body).Data);
    })
};
getAll();