/**
 * Created by jhenley on 2/14/2017.
 */
import Hoek from 'hoek'
import EventEmitter from 'events'
function register(server, options, next) {
    var emitter = new EventEmitter();
    server.on('tail', (req) => {
        var servent = {
            verb: Hoek.reach(req, 'method'),
            status: Hoek.reach(req, 'response.statusCode'),
            host: Hoek.reach(req, 'info.host'),
            path: req.path,
            hapi_id: req.id,
            req_headers: Hoek.clone(Hoek.reach(req, 'headers', {default: {}})),
            res_headers: Hoek.clone(Hoek.reach(req, 'response.headers', {default: {}})),

            status_class: `${Math.trunc(Hoek.reach(req, 'response.statusCode', {default: 0}) / 100)}00s`,
            timestamp: new Hoek.Timer().ts / 1000,
            datestamp: new Date(Date.now()).toISOString()
        };

        emitter.emit('servent', servent);
    });
    // server.expose('serventEmitter', () => emitter);
    server.decorate('server', 'serventEmitter', emitter);
    next();
};
register.attributes = {
    name: 'servent-master',
    version: '1.0.0'
};
export default register;