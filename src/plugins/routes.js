function register(server, options, next) {
    server.route({
        method: 'GET',
        path: '/suck-town',
        config: {
            handler: function(request, reply) {
                reply({
                    x: 42
                });
            }
        }
    });
    next();
};
register.attributes = {
    name: 'gql-routes',
    version: '1.0.0'
};
export default register;