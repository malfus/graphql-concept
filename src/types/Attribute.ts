/**
 * Created by jhenley on 2/22/2017.
 */
export interface Attribute {
    Name: string
    Value: string
}