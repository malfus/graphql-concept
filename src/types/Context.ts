import {OrganizationService} from "../services/organization";
import {TerritoryService} from "../services/territory";
import {ProgramService} from "../services/program";
import {AzureSearchService} from "../services/azureSearch";
import {LocationService} from "../services/location";
import {AddressService} from "../services/address";
/**
 * Created by jhenley on 2/22/2017.
 */
export interface Context {
    Address: AddressService,
    Organization: OrganizationService,
    Territory: TerritoryService,
    Program: ProgramService,
    AzureSearch: AzureSearchService,
    Location: LocationService
}