import {LegacyInformation} from "./LegacyInformation";
import {Attribute} from "./Attribute";
/**
 * Created by jhenley on 2/20/2017.
 */
export interface Organization {
    OrganizationID: string
    LegalName: string
    DoingBusinessAsName: string
    IsExternal: boolean
    MainType: string
    SubType: string
    InternalAliasName: string
    Status: string
    LastModifiedDate: string
    ParentOrganizationID: string
    LegacyInformation: LegacyInformation
    Attributes: [Attribute]
}
export interface OrgFilter {
    fLegalName?: string
    fLegacyInformation_LegacySystemName?: string
    fLegacyInformation_LegacySystemIdentifier?: string
    fAttributeName?: string
    fAttributeValue?: string
    sLegalName?: string
    sLegacyInformation_LegacySystemName?: string
    sLegacyInformation_LegacySystemIdentifier?: string
    pStart?: string
    pEnd?: string
}