import {LegacyInformation} from "./LegacyInformation";
import {RelatedOrganization, AssignedEntity} from "./RelatedEntities";
/**
 * Created by jhenley on 2/22/2017.
 */
export interface Territory {
    TerritoryID: string
    Description: string
    LastModifiedDate: string
    LegacyInformation: LegacyInformation
    RelatedOrganizations: [ RelatedOrganization ]
    AssignedEntities: [ AssignedEntity ]
    Boundaries: [ {
        BoundaryDefData: [string]
        BoundaryDefType: string
    } ]
}
export interface TerritoryFilter {
    fTerritoryPostalCode?: string
    fAssignedEntities_EntityID?: string
    fRelatedOrganizations_OrganizationID?: string
    pStart?: string
    pEnd?: string
}