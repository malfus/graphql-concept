/**
 * Created by jhenley on 3/16/2017.
 */
export interface Address {
    AddressType: AddressType
    ID: string
    Address1: string
    Address2: string
    City: string
    State: string
    PostalCode: string
    Country: string
    FullAddress: string
    ValidationError: string
    VerifyLevel: string
    Prompt: string
    Metadata: AddressMetaData
}
export interface AddressMetaData {
    Precision: string
    Location: LatLong
}
export interface LatLong {
    Latitude: string
    Longitude: string
}
export type AddressType = '0' | '1' | '2'
export interface AddressFilter {
    addressLine?: string
    city?: string
    state?: string
    postalCode?: string
}