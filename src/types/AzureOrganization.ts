/**
 * Created by jhenley on 3/15/2017.
 */
export interface AzureOrganization {
    "@search.score": number,
    id: string,
    enterpriseNumber: string
    licenseNumber: string
    enterpriseDBA: string
    licenseDBA: string
    legalName: string
    address: string
    city: string
    state: string
    zip: string
    phone: string
    extension: string
    url: string
}