import {Organization} from "./Organization";
/**
 * Created by jhenley on 2/24/2017.
 */
export interface ProgramDefinition {
    ProgramDefinitionID: string
    Name: string
    Description: string
    AdministeringOrganizationID: string
    SponsorOrganizationID: string
    RequiredForProductIDs: [ string ]
    QualificationNames: [ string ]
    LastModifiedDate: string
}
export interface ProgramDefinitionFilter {
    fName?: string
    pStart?: string
    pEnd?: string
}