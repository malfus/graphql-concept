/**
 * Created by jhenley on 2/22/2017.
 */
export interface LegacyInformation {
    LegacySystemName: string
    LegacySystemIdentifier: string
}