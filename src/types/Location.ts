import {Attribute} from "./Attribute";
import {RelatedOrganization} from "./RelatedEntities";
/**
 * Created by jhenley on 2/22/2017.
 */
export interface Location {
    LocationID: string
    Name: string
    Type: string
    AddressID: string
    LastModifiedDate: string
    RelatedOrganizations: [ RelatedOrganization ]
    Attributes: [ Attribute ]
}
export interface LocationFilter {
    fRelatedOrganizationID?: string
    fRelatedOrganizationType?: string
    fAttribute_Name?: string
    fAttribute_Value?: string
    fType?: string
    sName?: string
    pStart?: string
    pEnd?: string
}