/**
 * Created by jhenley on 2/22/2017.
 */
export interface RelatedOrganization {
    OrganizationID: string
    RelationshipType: string
}
export interface AssignedEntity {
    EntityType: string
    EntityID: string
    TerritoryType: string
}