import {Context} from "../types/Context";
import {ProgramDefinition} from "../types/Program";
/**
 * Created by jhenley on 2/23/2017.
 */
var resolverMap = {
  ProgramDefinition: {
    id: obj => obj.ProgramDefinitionID,
    name: obj => obj.Name,
    description: obj => obj.Description,
    administeringOrganization: (obj:ProgramDefinition, args, context: Context) => context.Organization.getById(obj.AdministeringOrganizationID),
    sponsorOrganization: (obj:ProgramDefinition, args, context: Context) => context.Organization.getById(obj.SponsorOrganizationID)
  }
};
export default resolverMap;