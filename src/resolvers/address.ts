import {Address} from "../types/Address";
/**
 * Created by jhenley on 3/16/2017.
 */
export const addressResolvers = {
    Address: {
        id: (obj: Address) => obj.ID
    }
};