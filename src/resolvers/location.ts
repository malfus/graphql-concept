import {Location} from "../types/Location";
import {Context} from "../types/Context";
import {RelatedOrganization} from "../types/RelatedEntities";
import * as Hoek from 'hoek'
import {resolverMap as relatedResolvers} from './relatedEntities';
import {resolverMap as attributeResolvers} from './atttribute';
/**
 * Created by jhenley on 3/16/2017.
 */
export var resolverMap = {
    Location: {
        id: (obj: Location) => obj.LocationID,
        relatedOrganizations: (obj: Location, args, context: Context) => {
            return obj.RelatedOrganizations.map((relatedOrg: RelatedOrganization) => context.Organization.getById(relatedOrg.OrganizationID));
        },
        address: (obj: Location, args, context: Context) => new Promise((resolve, reject) => {
            var addPromise = context.Address.getById(obj.AddressID);
            addPromise.then(data => resolve(data[0]));
            addPromise.catch(err => reject(err));
        })
    }
};
Hoek.merge(resolverMap, relatedResolvers);
Hoek.merge(resolverMap, attributeResolvers);