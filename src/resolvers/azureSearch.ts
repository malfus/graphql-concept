import {AzureOrganization} from "../types/AzureOrganization";
import {Context} from "../types/Context";
/**
 * Created by jhenley on 3/15/2017.
 */
export var resolverMap = {
    AzureOrganization: {
        score: (obj: AzureOrganization) => obj['@search.score']
    },
    Query: {
        azureSearch: (obj, args, context: Context) => context.AzureSearch.doAzureSearch(args.brand, args.zipCode)
    }
};