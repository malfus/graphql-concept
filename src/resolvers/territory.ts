import {Territory, TerritoryFilter} from "../types/Territory";
import {Context} from "../types/Context";
import * as Hoek from 'hoek'
import {AssignedEntity} from "../types/RelatedEntities";
import {resolveEntity, resolverMap as RelatedResolvers} from "./relatedEntities";
/**
 * Created by jhenley on 2/22/2017.
 */
var resolveAssignedEntities = (obj: Territory, args, context:Context) => {
    return obj.AssignedEntities.map(assignedEntity => resolveEntity(assignedEntity, args, context));
};
var resolverMap = {
    Territory: {
        id: (obj: Territory) => obj.TerritoryID,
        assignedEntities: resolveAssignedEntities
    },
    Boundary: {},
    Query: {
        territoryByID: (obj, {id}, context: Context) => context.Territory.getById(id),
        territories: (obj, args: TerritoryFilter, context: Context) => context.Territory.getByFilter(args)
    }
};
Hoek.merge(resolverMap, RelatedResolvers);
export default resolverMap;