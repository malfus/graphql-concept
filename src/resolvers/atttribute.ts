/**
 * Created by jhenley on 2/22/2017.
 */
export var resolverMap = {
    Attribute: {
        name: obj => obj.Name,
        value: obj => obj.Value
    }
};
export default resolverMap;