import attributeResolvers from "./atttribute";
import legacyInfoResolvers from "./legacyInformation";
import * as Hoek from 'hoek';
import {OrgFilter, Organization} from "../types/Organization";
import {Attribute} from "../types/Attribute";
import {Context} from "../types/Context";
import {Location} from "../types/Location";
/**
 * Created by jhenley on 2/15/2017.
 */
var getOrganizationById = (id, context) => context.Organization.getById(id);
var getOrganizations = (obj, args: OrgFilter, context) => context.Organization.getByFilter(args);
var getAttributeValueForOrganization = (attributeName: string) => (obj: Organization) => {
    var attribute = obj.Attributes.filter((attribute: Attribute) => attribute.Name === attributeName)[0];
    return attribute ? attribute.Value : null;
};
var resolverMap = {
    Organization: {
        id: ({OrganizationID}) => OrganizationID,
        name: ({LegalName}) => LegalName,
        parentOrganization: ({ParentOrganizationID}, args, context) => getOrganizationById(ParentOrganizationID, context),
        legacyName: (obj: Organization) => obj.LegacyInformation ? obj.LegacyInformation.LegacySystemName : null,
        legacyID: (obj: Organization) => obj.LegacyInformation ? obj.LegacyInformation.LegacySystemIdentifier : null,
        trades: getAttributeValueForOrganization('trades'),
        locations: (obj: Organization, args, context: Context) => context.Location.getByFilter({fRelatedOrganizationID: obj.OrganizationID}),
        addresses: (obj: Organization, args, context: Context) => {
            return new Promise((resolve1, reject1) => {
                context.Location.getByFilter({fRelatedOrganizationID: obj.OrganizationID}).then(
                    data1 => resolve1(Promise.all(data1.map(
                        loc => new Promise((resolve, reject) => {
                            var addPromise = context.Address.getById(loc.AddressID);
                            addPromise.then(data => resolve(data[0]));
                            addPromise.catch(err => reject(err));
                        }))
                    ))
                );
            });
        }

    }, Query: {
        organizationByID: (obj, args, context) => getOrganizationById(args.id, context),
        organizations: getOrganizations
    }
};
Hoek.merge(resolverMap, attributeResolvers);
Hoek.merge(resolverMap, legacyInfoResolvers);
export default resolverMap;