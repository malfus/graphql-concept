/**
 * Created by jhenley on 2/22/2017.
 */
var resolverMap = {
    LegacyInformation: {
        name: obj => obj.LegacySystemName,
        id: obj => obj.LegacySystemIdentifier
    }
};
export default resolverMap;