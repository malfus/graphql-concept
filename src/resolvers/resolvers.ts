/**
 * Created by jhenley on 2/23/2017.
 */
import * as Hoek from "hoek";
import territoryResolvers from "./territory";
import organizationResolvers from "./organization";
import programResolvers from "./program";
import {resolverMap as azureSearchResolvers} from "./azureSearch";
import {resolverMap as locationResolvers} from "./location";
import {addressResolvers} from "./address";
export var resolverMap = {};
Hoek.merge(resolverMap, organizationResolvers);
Hoek.merge(resolverMap, territoryResolvers);
Hoek.merge(resolverMap, programResolvers);
Hoek.merge(resolverMap, azureSearchResolvers);
Hoek.merge(resolverMap, locationResolvers);
Hoek.merge(resolverMap, addressResolvers);
export default resolverMap;
