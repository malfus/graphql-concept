import {Context} from "../types/Context";
import {RelatedOrganization, AssignedEntity} from "../types/RelatedEntities";
/**
 * Created by jhenley on 2/22/2017.
 */
export function resolveEntity(obj: AssignedEntity, args, context: Context) {
    if (obj.EntityType === 'License') {
        return context.Organization.getById(obj.EntityID);
    } else if (obj.EntityType === 'ProgramDefinition') {
        return context.Program.getById(obj.EntityID);
    } else {
        return {};
    }
};
var resolveType = obj => {
    if(obj.DoingBusinessAsName)
        return 'Organization';
    else if (obj.ProgramDefinitionID)
        return 'ProgramDefinition';
    else
        return 'Unknown';
};
export var resolverMap = {
    RelatedOrganization: {
        id: (obj: RelatedOrganization) => obj.OrganizationID,
        type: (obj: RelatedOrganization) => obj.RelationshipType,
        organization: (obj: RelatedOrganization, args, context: Context) => context.Organization.getById(obj.OrganizationID)
    },
    AssignedEntity: {
        entity: resolveEntity
    },
    Entity: {
        __resolveType: resolveType
    },
    Unknown: {
        message: () => 'Unknown entity'
    }
};